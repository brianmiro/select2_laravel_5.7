<html lang="en">


<head>
  <title>Laravel 5 - Dynamic autocomplete search using select2 JS Ajax</title>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
</head>


<body>


  <div class="container">

    <h2>Laravel 5 - Autocompletar Dinamico usando select2 JS Ajax</h2>
    <br />
    <select id="nombre" class="form-control" style="width:500px;"></select>
    <select id="nombre_multiple" class="form-control" style="width:500px;" multiple="multiple" name="names[]"></select>
    <select id="dias" class="form-control" style="width:500px;" multiple="multiple" name="dias_select[]">
      <option value="LUNES">Lunes</option>
      <option value="MARTES">Martes</option>
      <option value="MIERCOLES">Miercoles</option>
      <option value="JUEVES">Jueves</option>
      <option value="VIERNES">Viernes</option>
      <option value="SABADO">Sabado</option>
      <option value="DOMINGO">Domingo</option>
    </select>

    <button id="btn" class="btn-info form-control">Guardar Dias</button>

  </div>


  <script type="text/javascript">
    var arrayDias = [];
    $('#nombre').select2({
      placeholder: 'Seleccione una opcion',
      ajax: {
        url: '/select2-autocomplete-ajax',
        dataType: 'json',
        delay: 250,
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.name,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
    $('#nombre_multiple').select2({
      placeholder: 'Seleccione una opcion',
      maximumSelectionLength: 7,
      ajax: {
        url: '/select2-autocomplete-ajax',
        dataType: 'json',
        delay: 250,
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.name,
                id: item.id
              }
            })
          };
        },
        cache: true
      }
    });
    $('#dias').select2({
      placeholder: 'Seleccione los dias',
      maximumSelectionLength: 7,
      /*ajax: {
        url: '/select2-autocomplete-ajax',
        dataType: 'json',
        delay: 250,
        processResults: function(data) {
          return {
            results: $.map(data, function(item) {
              return {
                text: item.name,
                id: item.id
              }
            })
          };
        },
        cache: true
      }*/
    });

    $("#dias").change(function(event) {
      var id = $("#dias").find(':selected').val();
      var dia = $("#dias option").find(':selected').attr('value');

      console.log(id);
      console.log(dia);

    });
    $('#btn').click(function() {

      var values = $('#dias').val().toString();


console.log(values);
    });
  </script>


</body>

</html>